library playground;

import "dart:html";

class Control {
	int _jump;
	Control(String txt,this._jump, String hoverText) {
		DivElement elm = new DivElement();

		int posunuti = 0;


		document.body.append(elm);
		elm.text = txt;
		elm.style.position = "absolute";
		elm.style.top = "0px";
		elm.style.transition = "top 1s";


		DivElement hElm = new DivElement();


		elm.onClick.listen((MouseEvent e) {
			elm.style.top = "200px";
			/* pokud bude odjizdet element, schovame hover element */
			hElm.style.display = "none";
		});


		elm.onMouseOver.listen((MouseEvent e) {
			hElm.style.display = "block";
			/* musime patricne posunout take hover element, protoze uzivatel mohl s puvodnim hnout - elm.onClick */
			posunuti = elm.offsetTop + 20;
			hElm.style.top = posunuti.toString() + "px";
		});
		elm.onMouseLeave.listen((MouseEvent e) {
			hElm.style.display = "none";
		});

		document.body.append(hElm);
		hElm.style.display = "none";
		hElm.style.border = "solid 1px #B18904";
		hElm.style.backgroundColor = "#F2F5A9";
		hElm.style.padding = "4px";
		hElm.style.textAlign = "center";

		/*
		if (hoverText.toLowerCase().indexOf("<") > -1) hElm.innerHtml = hoverText;
	  else hElm.text = hoverText;
	  */
		hElm.innerHtml = hoverText;
    if (hoverText.length > 30) {
			hElm.style.width = "40%";
		} else {
			hElm.style.width = "20%";
		}

		posunuti = 20;

		hElm.style.top = posunuti.toString() + "px";
		hElm.style.position = "absolute";


	}
}